import gist
import numpy as np
import os
import cv2


inputDir = "./VA/"
outputDir = "."

for split in ["Train", "Val", "Test"]:

    dataArr = None
    indecies = []
    filenames = []

    dirPath = os.path.join(inputDir, split)
    files = sorted(os.listdir(dirPath))
    for idx, imgFile in enumerate(files):

        if idx % 250 == 0:
            print(split, idx, len(files))

        imgPath = os.path.join(dirPath, imgFile)
        img = cv2.imread(imgPath)

        imgGray = np.mean(img, axis=2)
        img[:,:,0] = imgGray
        img[:,:,1] = imgGray
        img[:,:,2] = imgGray

        img = cv2.resize(img, (128, 128)).astype(np.uint8)

        descriptor = gist.extract(img, nblocks=4, orientations_per_scale=(8, 8, 8, 8))

        indecies.append(idx)
        filenames.append(imgFile)
        if dataArr is None:
            dataArr = descriptor[:512]
        else:
            dataArr = np.vstack((dataArr, descriptor[:512]))

    np.save(os.path.join(outputDir, split+"_Data.npy"), dataArr)
    np.save(os.path.join(outputDir, split+"_Index.npy"), indecies)
    np.save(os.path.join(outputDir, split+"_Filename.npy"), filenames)