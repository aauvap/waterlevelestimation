import os
import pandas as pd


inputDir = "./VAData"

aggregator = {}
columns = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

reports = []

for split in ["Train", "Val", "Test"]:
    df = pd.read_csv(os.path.join(inputDir, "ALL_VA_{}.csv".format(split)), sep=",")

    for level in columns:
        df_level = df[df["WaterLevel"] == level]

        if level in aggregator.keys():
            aggregator[level].extend([len(df_level), len(df_level["Report"].unique())])
        else:
            aggregator[level] = [len(df_level), len(df_level["Report"].unique())]
    
    reports.extend(df["Report"])
    if "Total" in aggregator.keys():
        aggregator["Total"].extend([len(df), len(df["Report"].unique())])
    else:
        aggregator["Total"] = [len(df), len(df["Report"].unique())]

columns += ["Total"]
print(aggregator)

print(len(list(set(reports))))

for key in aggregator.keys():
    vals = aggregator[key]
    aggregator[key].extend([vals[0]+vals[2]+vals[4], vals[1]+vals[3]+vals[5]])


print(aggregator)

df = pd.DataFrame.from_dict(aggregator, orient="index")
df.to_csv("data_stats.csv", sep=",")