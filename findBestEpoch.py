import os
import json
import numpy as np
import matplotlib.pyplot as plt

inputDir = "./WaterLevel_runs"


subdirs = [x for x in os.listdir(inputDir) if os.path.isdir(os.path.join(inputDir, x))]
epochs = [x for x in range(1, 51)]

for subdir in sorted(subdirs):
    resultsFile = os.path.join(inputDir, subdir, "Results.json")

    if os.path.isfile(resultsFile):
        with open(resultsFile) as json_file:
            results = json.load(json_file)
        
        vaL_metric = results["ValMetric"]
        vaL_loss = results["ValLoss"]
        train_metric = results["TrainMetric"]
        train_loss = results["TrainLoss"]

        if "Regression" in subdir:
            print(subdir, np.max(vaL_metric), np.argmax(vaL_metric))
        else:
            print(subdir, np.min(vaL_metric), np.argmin(vaL_metric))
        
        print(subdir, np.min(vaL_loss), np.argmin(vaL_loss))
        print()

        plt.figure(figsize=(12,6))
        plt.subplot(1,2,1)
        plt.plot(epochs, vaL_metric, label="Validation-Metric")
        plt.plot(epochs, train_metric, label="Train-Metric")
        plt.grid()
        plt.title("Metric")
        plt.xlabel("Epochs")

        if "Regression" in subdir:
            plt.ylabel("MSE")
        else:
            plt.ylabel("Accuracy")

        plt.legend(loc="upper left")

        plt.subplot(1,2,2)
        plt.plot(epochs, vaL_loss, label="Validation-Loss")
        plt.plot(epochs, train_loss, label="Train-Loss")
        plt.grid()
        plt.title("Loss")
        plt.xlabel("Epochs")

        if "Regression" in subdir:
            plt.ylabel("MSE-Loss")
        else:
            plt.ylabel("CE-Loss")

        plt.legend(loc="upper left")

        plt.savefig(os.path.join(".", subdir+".png"))
        