python classificationAnalysis.py -id ./ResultsCNN/results_loss -od ./Results/results_loss
python classificationAnalysis.py -id ./ResultsCNN/results_metric -od ./Results/results_metric

python classificationAnalysis.py -id ./ResultsTree/results_loss -od ./Results/results_loss
python classificationAnalysis.py -id ./ResultsTree/results_metric -od ./Results/results_metric

python regressionAnalysis.py -id ./ResultsCNN/results_loss -od ./Results/results_loss
python regressionAnalysis.py -id ./ResultsCNN/results_metric -od ./Results/results_metric

python regressionAnalysis.py -id ./ResultsTree/results_loss -od ./Results/results_loss
python regressionAnalysis.py -id ./ResultsTree/results_metric -od ./Results/results_metric