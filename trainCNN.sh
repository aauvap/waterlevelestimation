python trainCNN.py --data ./VA --model alexnet --mode Class2011  --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Class2011 --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Class2011 --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Class2011 --batch-size 64 --lr 0.01 --epochs 50

python trainCNN.py --data ./VA --model alexnet --mode Class2011 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Class2011 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Class2011 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Class2011 --pretrained --batch-size 64 --lr 0.001 --epochs 50

python trainCNN.py --data ./VA --model alexnet --mode Class2015  --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Class2015 --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Class2015 --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Class2015 --batch-size 64 --lr 0.01 --epochs 50

python trainCNN.py --data ./VA --model alexnet --mode Class2015 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Class2015 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Class2015 --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Class2015 --pretrained --batch-size 64 --lr 0.001 --epochs 50

python trainCNN.py --data ./VA --model alexnet --mode Regression  --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Regression --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Regression --batch-size 64 --lr 0.01 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Regression --batch-size 64 --lr 0.01 --epochs 50

python trainCNN.py --data ./VA --model alexnet --mode Regression --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet18 --mode Regression --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet34 --mode Regression --pretrained --batch-size 64 --lr 0.001 --epochs 50
python trainCNN.py --data ./VA --model resnet50 --mode Regression --pretrained --batch-size 64 --lr 0.001 --epochs 50