import os
import json
import argparse
import itertools
import sklearn.metrics
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def calcualte_classification_metrics(predictions, labels, num_classes, num_cases):

    cm = sklearn.metrics.confusion_matrix(labels, predictions)

    n_GT = np.zeros((num_classes)) # Number of ground truth cases
    n_PD = np.zeros((num_classes)) # Number of predictions
    n_TP = np.zeros((num_classes)) # Number of True Positives
    n_TN = np.zeros((num_classes)) # Number of True Negatives
    n_FP = np.zeros((num_classes)) # Number of False Positives
    n_FN = np.zeros((num_classes)) # Number of False Negatives

    for idx, label in enumerate(range(num_classes)):
        label_predicted = predictions == label
        label_target = labels == label

        n_GT[idx] = np.sum(label_target)
        n_PD[idx] = np.sum(label_predicted)

        n_TP[idx] = np.sum(label_predicted * label_target)
        n_TN[idx] = np.sum(label_predicted == label_target) - n_TP[idx]

        n_FP[idx] = n_PD[idx] - n_TP[idx]
        n_FN[idx] = num_cases - n_TN[idx] - n_FP[idx]- n_TP[idx]


    # If Np is 0 for any class, set to 1 to avoid division with 0
    n_PD[n_PD == 0] = 1

    # Overall Precision, Recall and F1
    OP = np.sum(n_TP) / np.sum(n_PD)
    OR = np.sum(n_TP) / np.sum(n_GT)
    OF1 = (2 * OP * OR) / (OP + OR)

    # Per-Class Precision, Recall and F1
    CP = np.sum(n_TP / n_PD) / num_classes
    CR = np.sum(n_TP / n_GT) / num_classes
    CF1 = (2 * CP * CR) / (CP + CR)

    # Macro F1
    precision_k = n_TP / n_PD
    recall_k = n_TP / n_GT
    F1_k = (2 * precision_k * recall_k)/(precision_k + recall_k)

    F1_k[F1_k != F1_k] = 0

    MF1 = np.sum(F1_k)/num_classes

    # Micro F1
    mPrecision = np.sum(n_TP) / (np.sum(n_TP) + np.sum(n_FP))
    mRecall = np.sum(n_TP) / (np.sum(n_TP) + np.sum(n_FN))
    mF1 = (2*mPrecision*mRecall)/(mPrecision+mRecall) #(2 * np.sum(n_TP)) / (np.sum(n_PD) + np.sum(n_GT))


    # Macro Accuracy
    accuracy_k = (n_TP + n_TN) / (n_TP + n_TN + n_FP + n_FN)
    Maccuracy = np.sum(accuracy_k) / num_classes

    # Micro Accuracy
    maccuracy = (np.sum(n_TP) + np.sum(n_TN)) / (np.sum(n_TP) + np.sum(n_TN) + np.sum(n_FN) + np.sum(n_FP))




    print(num_cases)
    print(n_GT)
    print(n_PD)
    print(n_TP)
    print(n_TN)
    print(n_FP)
    print(n_FN)
    print("Accuracy @ k: ", accuracy_k)
    print("Precision @ k: ", precision_k)
    print("Recall @ k: ", recall_k)
    print("F1 @ k: ", F1_k)
    print("Overall F1: ", OF1)
    print("PerClass F1: ", CF1)
    print("Macro F1: ", MF1)
    print("Micro F1: ", mF1)
    print("Micro Accuracy: ", maccuracy)
    print("Macro Accuracy: ", Maccuracy)

    stats = {"GroundTruth": list(n_GT),
             "Predictions": list(n_PD),
             "TruePositive": list(n_TP),
             "TrueNegative": list(n_TN),
             "FalsePositive": list(n_FP),
             "FalseNegative": list(n_FN)}
    metrics = {"Accuracy@k": list(accuracy_k),
               "Recall@k": list(recall_k),
               "Precision@k": list(precision_k),
               "F1@k": list(F1_k),
               "OverallF1": OF1,
               "PerClassF1": CF1,
               "MicroF1": mF1,
               "MacroF1": MF1,
               "MicroPrecision": mPrecision,
               "MicroRecall": mRecall,
               "MicroAccucary": maccuracy,
               "MacroAccuracy": Maccuracy}

    return stats, metrics, cm


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          filename="ConfusionMatrix",
                          outputDir=".",
                          macroF1 = None,
                          microF1 = None,
                          cmap=None,
                          normalize=True):

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(16, 10))
    plt.title(title)
    
    cm_copy = cm.copy()
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        climMax = 1
    else:
        climMax = np.max(cm.sum(axis=1))

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    
    
    plt.clim(0, climMax)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)


    thresh = climMax / 2 if normalize else climMax / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            if True:#j == i:
                plt.text(j, i, "{:.2f}%\n({}/{})".format(cm[i, j]*100, cm_copy[i, j], cm_copy[i,:].sum()),
                        horizontalalignment="center",
                        color="white" if cm[i, j] > thresh else "black")
            else:
                plt.text(j, i, "{:.2f}%".format(cm[i, j]*100),
                        horizontalalignment="center",
                        color="white" if cm[i, j] > thresh else "black")
                
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('True label')
    
    if macroF1 is None or microF1 is None:
        plt.xlabel('Predicted label')
    else:
        plt.xlabel("Predicted label\n Macro F1={:0.4f}; Micro F1={:0.4f}".format(macroF1, microF1))
    plt.savefig(os.path.join(outputDir, "{}.pdf".format(filename)),
               bbox_inches='tight', 
               transparent=True)
    plt.savefig(os.path.join(outputDir, "{}.png".format(filename)),
               bbox_inches='tight', 
               transparent=True)



if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-id", "--inputDir", required=True, help="Path to input dir")
    ap.add_argument("-od", "--outputDir", required=True, help="Path to output dir")
    args = vars(ap.parse_args())

    inputDir = args["inputDir"]
    outputDir = args["outputDir"]


    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)

    for subdir, dirs, files in os.walk(inputDir):
        for inputFile in files:

            if "Regression" in inputFile:
                continue

            inputBaseName = os.path.splitext(inputFile)[0]
            csvPath = os.path.join(inputDir, inputFile)
            df = pd.read_csv(csvPath, sep=",")

            predictions = df["Prediction"].values
            labels = df["Label"].values

            num_classes = np.max(labels)+1
            num_cases = len(labels)

            stats, metrics, cm = calcualte_classification_metrics(predictions, labels, num_classes, num_cases)


            plot_confusion_matrix(cm, [x for x in range(num_classes)], filename=inputBaseName+"_cm", title=inputBaseName, outputDir = outputDir,  normalize=True, macroF1=metrics["MacroF1"], microF1=metrics["MicroF1"])

            with open(os.path.join(outputDir, '{}_class_stats.json'.format(inputBaseName)), 'w') as outfile:
                json.dump(stats, outfile)

            with open(os.path.join(outputDir, '{}_class_metrics.json'.format(inputBaseName)), 'w') as outfile:
                json.dump(metrics, outfile)

            np.save(os.path.join(outputDir, '{}_confusion_matrix.npy'.format(inputBaseName)), cm)