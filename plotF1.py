import os 
import json
import matplotlib.pyplot as plt
import numpy as np
#plt.style.use('ggplot2')

inputPath = "./Results/results_loss"

dirContent = os.listdir(inputPath)
dirContent = [x for x in dirContent if "class_metric" in x]
print(dirContent)

dataDir = {}

for split in ["Class2011", "Regression", "Class2015"]:
    dataDir[split] = {}
    for f in dirContent:

        if split not in f:
            continue

        if "False" in f: #False == From scratch, True == Pretrained
            continue
        
        method = f.split("_")[0]
        method = method.split("-")[0]

        method = method.replace("resnet", "ResNet")
        method = method.replace("alexnet", "AlexNet")
        method = method.replace("ExtraTree", "Extra Trees")
        method = method.replace("RandomForest", "Random Forest")

        if "ResNet" in method or "AlexNet" in method:
            method += "-FT"
        
        with open(os.path.join(inputPath, f), 'r') as f:
            class_metrics = json.load(f)
        
        dataDir[split][method] = class_metrics["F1@k"]


print(dataDir)


order = ["Random Forest", "Extra Trees", "AlexNet-FT", "ResNet18-FT", "ResNet34-FT", "ResNet50-FT"]
levels = {"Class2011": [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100], "Regression": [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100], "Class2015": [0, 1, 2, 3]}
names = {"Class2011": ["0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%"], "Regression": ["0%", "10%", "20%", "30%", "40%", "50%", "60%", "70%", "80%", "90%", "100%"], "Class2015": [r"$x < 5\%$", r"$5\% \leq x < 15\%$", r"$15\% \leq x < 30\%$", r"$30\% \leq x$"]}
colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:brown"]
linestyles = ["solid", "dotted", "dashed", "dashdot", (0, (5, 1)), (0, (3, 5, 1, 5))]
markers = ['o', 'o', 's', '^', '^', '^']
for split in ["Class2011", "Regression", "Class2015"]:

    metrics= dataDir[split]
    level = levels[split]
    name = names[split]

    plt.figure(figsize=(8,6))

    plt.tight_layout()

    for method_idx, method in enumerate(order):
        data = metrics[method]

        plt.plot(level, [x*100 for x in data], label=method, color = colors[method_idx], linestyle=linestyles[method_idx], zorder=1, marker = markers[method_idx])
        #plt.scatter(level, [x*100 for x in data], color = colors[method_idx], zorder=2, s = 20, marker)

    plt.ylim(-1.5, 101.5)
    plt.xlim(min(level)-1.5, max(level)+1.5)
    plt.ylabel("F1 Score (%)")
    plt.xlabel("Water Level")
    plt.xticks(level,name)

    if split is "Class2015":    
        x_major_ticks = np.arange(0, 4, 1)
        major_ticks = np.arange(0, 101, 10)
        minor_ticks = np.arange(0, 101, 5)
        plt.gca().set_xticks(x_major_ticks)
        plt.gca().set_yticks(major_ticks)
        plt.gca().set_yticks(minor_ticks, minor=True)
    
    else:
        major_ticks = np.arange(0, 101, 10)
        minor_ticks = np.arange(0, 101, 5)

        plt.gca().set_xticks(major_ticks)
        plt.gca().set_xticks(minor_ticks, minor=True)
        plt.gca().set_yticks(major_ticks)
        plt.gca().set_yticks(minor_ticks, minor=True)
    # And a corresponding grid
    plt.gca().grid(which='both')

    # Or if you want different settings for the grids:
    plt.gca().grid(which='minor', alpha=0.2)
    plt.gca().grid(which='major', alpha=0.5)

    #plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    #plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.0), ncol=2, fancybox=True, shadow=True)

    name = split
    name = name.replace("Class2011", "Class10")
    name = name.replace("Regression", "Reg2Class10")
    name = name.replace("Class2015", "Class15")

    plt.title("F1-metric per class - {}".format(name))

    plt.savefig("./F1_{}.pdf".format(name),
               bbox_inches='tight', 
               transparent=True)
    plt.close()





plt.figure(figsize=(14,6))

plt.tight_layout()


for idx, split in enumerate(["Class2011", "Regression", "Class2015"]):

    metrics= dataDir[split]
    level = levels[split]
    name = names[split]

    plt.subplot(1,3,idx+1)

    for method_idx, method in enumerate(order):
        data = metrics[method]

        plt.plot(level, [x*100 for x in data], label=method, color = colors[method_idx], linestyle=linestyles[method_idx], zorder=1, marker = markers[method_idx])
        #plt.scatter(level, [x*100 for x in data], color = colors[method_idx], zorder=2, s = 20)

    plt.ylim(-1.5, 101.5)
    plt.xlim(min(level)-1.5, max(level)+1.5)
    if idx == 0:
        plt.ylabel("F1 Score (%)")
    plt.xlabel("Water Level")
    plt.xticks(level,name, rotation=45)

    if split is "Class2015":    
        plt.xlim(min(level)-0.15, max(level)+0.15)
        x_major_ticks = np.arange(0, 4, 1)
        major_ticks = np.arange(0, 101, 10)
        minor_ticks = np.arange(0, 101, 5)
        plt.gca().set_xticks(x_major_ticks)
        plt.gca().set_yticks(major_ticks)
        plt.gca().set_yticks(minor_ticks, minor=True)
    
    else:
        major_ticks = np.arange(0, 101, 10)
        minor_ticks = np.arange(0, 101, 5)

        plt.gca().set_xticks(major_ticks)
        plt.gca().set_xticks(minor_ticks, minor=True)
        plt.gca().set_yticks(major_ticks)
        plt.gca().set_yticks(minor_ticks, minor=True)
    # And a corresponding grid
    plt.gca().grid(which='both')

    # Or if you want different settings for the grids:
    plt.gca().grid(which='minor', alpha=0.2)
    plt.gca().grid(which='major', alpha=0.5)

    #plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
    #plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    if idx ==1:
        plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.0), ncol=2, fancybox=True, shadow=True)

    name = split
    name = name.replace("Class2011", "Class10")
    name = name.replace("Regression", "Reg2Class10")
    name = name.replace("Class2015", "Class15")

    plt.title("F1-metric per class - {}".format(name))

plt.savefig("./F1_ALL.pdf",
            bbox_inches='tight', 
            transparent=True)
