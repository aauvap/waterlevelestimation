import os
import json
import pandas as pd


modelType = "loss"

inputDir = "./Results/results_{}".format(modelType)
aggregatorF1 = {}
columns = []

for setting in ["Class2011", "Regression", "Class2015"]:
    columns.extend(["MicroF1-{}".format(setting),"MacroF1-{}".format(setting)])

        
    aggregatorF1Class = {}
    columnsF1Class = []
    for inputFile in sorted(os.listdir(inputDir)):

        parts = os.path.splitext(inputFile)

        if parts[-1] != ".json":
            continue
        
        if "class_metrics" not in parts[0]:
            continue
        
        if setting not in parts[0]:
            continue

        print(inputFile)

        method = inputFile.split("_")[0]
        method = method.split("-")[0]


        with open(os.path.join(inputDir, inputFile), 'r') as f:
            class_metrics = json.load(f)

        if "True" in inputFile:
            key = method + "-FT"
        else:
            key = method + "-S"

        if key in aggregatorF1.keys():
            aggregatorF1[key].extend(["{:.2f}".format(class_metrics["MicroF1"]*100), "{:.2f}".format(class_metrics["MacroF1"]*100)])
        else:
            aggregatorF1[key] = ["{:.2f}".format(class_metrics["MicroF1"]*100), "{:.2f}".format(class_metrics["MacroF1"]*100)]
            
        classF1 = class_metrics["F1@k"]
        aggregatorF1Class[key] = ["{:.2f}".format(classF1[x]*100) for x in range(len(classF1))]
    
    columnsF1Class = [x for x in range(len(classF1))]
    
    df = pd.DataFrame.from_dict(aggregatorF1Class,orient='index', columns=columnsF1Class)
    df.to_csv("test_F1Class_{}_{}.csv".format(modelType, setting), sep=",")

df = pd.DataFrame.from_dict(aggregatorF1,orient='index', columns=columns)
df.to_csv("test_F1_{}.csv".format(modelType), sep=",")