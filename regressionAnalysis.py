import os
import json
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from classificationAnalysis import calcualte_classification_metrics, plot_confusion_matrix


def plot_regression_fit(predictions, labels, outputDir = ".", filename ="RegressionFit", title = "Regression Fit", mse=None, mae=None, cmap=None):

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(16, 10))

    plt.tight_layout()

    plt.scatter(labels, predictions)

    plt.plot([0, 100], [0, 100], color="black")

    plt.xlim(-0.5, 100.5)
    plt.ylim(-0.5, 100.5)

    plt.gca().set_xticks([0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])
    plt.gca().set_yticks([0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])

    plt.grid(True)
    plt.title(title)
    
    plt.ylabel('Predicted label')
    if mae is None or mse is None:
        plt.xlabel('True label')
    else:
        plt.xlabel("True label\n MAE={:0.4f}; MSE={:0.4f}".format(mae, mse))
    plt.savefig(os.path.join(outputDir, "{}.pdf".format(filename)),
               bbox_inches='tight', 
               transparent=True)
    plt.savefig(os.path.join(outputDir, "{}.png".format(filename)),
               bbox_inches='tight', 
               transparent=True)


def calculate_regression_metrics(predictions, labels):

    mae = np.mean(np.abs(predictions-labels))
    mse = np.mean((predictions-labels)**2)

    return {"MAE": mae, "MSE": mse}


def convert_to_classification(predictions, classes=[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]):
    class_predictions = np.zeros(predictions.shape, dtype=np.int)

    for idx in range(class_predictions.shape[0]):
        class_predictions[idx] = int(np.argmin(np.abs(predictions[idx]-classes)))

    return class_predictions



if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-id", "--inputDir", required=True, help="Path to input dir")
    ap.add_argument("-od", "--outputDir", required=True, help="Path to output dir")
    args = vars(ap.parse_args())

    inputDir = args["inputDir"]
    outputDir = args["outputDir"]


    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)

    for subdir, dirs, files in os.walk(inputDir):
        for inputFile in files:

            if "Class"  in inputFile:
                continue

            inputBaseName = os.path.splitext(inputFile)[0]

            csvPath = os.path.join(inputDir, inputFile)
            df = pd.read_csv(csvPath, sep=",")

            predictions = df["Prediction"].values
            labels = df["Label"].values


            regression_metrics = calculate_regression_metrics(predictions, labels)
            plot_regression_fit(predictions, labels, mae=regression_metrics["MAE"], mse=regression_metrics["MSE"], outputDir=outputDir, title=inputBaseName, filename=inputBaseName+"_reg")

            classification_predictions = convert_to_classification(predictions)
            classification_label_map = {x:idx for idx, x in enumerate(np.unique(labels))}
            classification_labels = np.asarray([classification_label_map[x] for x in labels])
            print(classification_label_map)


            num_classes = np.max(classification_labels)+1
            num_cases = len(classification_labels)
            print(classification_labels)
            print(classification_predictions)

            stats, metrics, cm = calcualte_classification_metrics(classification_predictions, classification_labels, num_classes, num_cases)

            plot_confusion_matrix(cm, [x for x in range(num_classes)], filename=inputBaseName+"_cm", title=inputBaseName, outputDir = outputDir,  normalize=True, macroF1=metrics["MacroF1"], microF1=metrics["MicroF1"])

            with open(os.path.join(outputDir, '{}_class_stats.json'.format(inputBaseName)), 'w') as outfile:
                json.dump(stats, outfile)

            with open(os.path.join(outputDir, '{}_class_metrics.json'.format(inputBaseName)), 'w') as outfile:
                json.dump(metrics, outfile)

            with open(os.path.join(outputDir, '{}_regression_metrics.json'.format(inputBaseName)), 'w') as outfile:
                json.dump(regression_metrics, outfile)

            np.save(os.path.join(outputDir, '{}_confusion_matrix.npy'.format(inputBaseName)), cm)

            