
import os
import pandas as pd
import numpy as np
 
import torch
from torch.utils.data import Dataset
from torchvision.datasets.folder import default_loader
 
class WaterClassificationDataset(Dataset):
    def __init__(self, root, split="Train", transform=None, loader=default_loader, waterIntervals = []):
        self.root = root
        self.imgDir = os.path.join(root, split)
        self.split = split
        self.transform = transform
        self.loader = default_loader
 
        self.waterIntervals = waterIntervals
 
        self.loadAnnotations()

        self.class_weights = self.calculate_label_weights()
 
    def loadAnnotations(self):
        gtPath = os.path.join(self.root, "ALL_VA_{}.csv".format(self.split))
        gt = pd.read_csv(gtPath, sep=",", encoding="utf-8", usecols = ["Filename", "WaterLevel"])
        self.imgPaths = gt["Filename"].values
        self.waterLevel = gt["WaterLevel"].values
        del gt
 
        if len(self.waterIntervals) > 0:
            self.numWaterClasses = len(self.waterIntervals)+1
            self.waterLevel[self.waterLevel < self.waterIntervals[0]] = 0
            self.waterLevel[self.waterLevel >= self.waterIntervals[-1]] = self.numWaterClasses-1
            for idx in range(1, len(self.waterIntervals)):
                self.waterLevel[(self.waterLevel >= self.waterIntervals[idx-1]) & (self.waterLevel < self.waterIntervals[idx])] = idx
        else:
            uniqueLevels = np.unique(self.waterLevel)
            self.numWaterClasses = len(uniqueLevels)
            for idx, level in enumerate(uniqueLevels):
                self.waterLevel[self.waterLevel == level] = idx
 
    def __len__(self):
        return len(self.imgPaths)
 
    def __getitem__(self, index):
        path = self.imgPaths[index]
        waterLevel = self.waterLevel[index]
        return self.loadImg(path, waterLevel)
 
    def loadImg(self, path, waterLevel):
        img = self.loader(os.path.join(self.imgDir, path))
        if self.transform is not None:
            img = self.transform(img)
 
        waterTarget = np.zeros(self.numWaterClasses, np.float32)
        waterTarget[waterLevel]
 
        return img, waterLevel, path


    def calculate_label_weights(self):

        unique_labels, unique_counts = np.unique(self.waterLevel, return_counts = True)
        largest_count = np.max(unique_counts)
        class_weights = largest_count / unique_counts

        return class_weights
 
class WaterClassificationRegression(Dataset):
    def __init__(self, root, split="Train", transform=None, loader=default_loader):
        self.root = root
        self.imgDir = os.path.join(root, split)
        self.split = split
        self.transform = transform
        self.loader = default_loader
        self.loadAnnotations()
 
    def loadAnnotations(self):
        gtPath = os.path.join(self.root, "ALL_VA_{}.csv".format(self.split))
        gt = pd.read_csv(gtPath, sep=",", encoding="utf-8", usecols = ["Filename", "WaterLevel"])
        self.imgPaths = gt["Filename"].values
        self.waterLevel = gt["WaterLevel"].values
        del gt
 
    def __len__(self):
        return len(self.imgPaths)
 
    def __getitem__(self, index):
        path = self.imgPaths[index]
        waterLevel = self.waterLevel[index]
        return self.loadImg(path, waterLevel)
 
    def loadImg(self, path, waterLevel):
        img = self.loader(os.path.join(self.imgDir, path))
        if self.transform is not None:
            img = self.transform(img)
 
        return img, np.asarray([waterLevel,]), path




if __name__ == "__main__":
    from torch.utils.data import DataLoader
    import torchvision.transforms as transforms
 
    
    transform = transforms.Compose(
        [transforms.Resize((256,256)),
        transforms.ToTensor()])
 
    inputDir = "VA"
    
    train = WaterClassificationDataset(root = inputDir, split="Train", transform=transform)
    trainloader = DataLoader(train, batch_size=20, shuffle=False, num_workers=0)
    print(len(train))
    for i, data in enumerate(trainloader, 0):
        print(data)
        break


    train = WaterClassificationDataset(root = inputDir, split="Train", transform=transform, waterIntervals=[5, 15, 30])
    trainloader = DataLoader(train, batch_size=20, shuffle=False, num_workers=0)
    print(len(train))
    for i, data in enumerate(trainloader, 0):
        print(data)
        break


    train = WaterClassificationRegression(root = inputDir, split="Train", transform=transform)
    trainloader = DataLoader(train, batch_size=20, shuffle=False, num_workers=0)
    print(len(train))
    for i, data in enumerate(trainloader, 0):
        print(data)
        break


