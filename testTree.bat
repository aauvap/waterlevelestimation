python testTree.py --modelPath ./TreeMethod/ExtraTree-Class2011/ --outputDir ./ResultsTree --mode Class2011
python testTree.py --modelPath ./TreeMethod/ExtraTree-Class2015/ --outputDir ./ResultsTree --mode Class2015
python testTree.py --modelPath ./TreeMethod/ExtraTree-Regression/ --outputDir ./ResultsTree --mode Regression

python testTree.py --modelPath ./TreeMethod/RandomForest-Class2011/ --outputDir ./ResultsTree --mode Class2011
python testTree.py --modelPath ./TreeMethod/RandomForest-Class2015/ --outputDir ./ResultsTree --mode Class2015
python testTree.py --modelPath ./TreeMethod/RandomForest-Regression/ --outputDir ./ResultsTree --mode Regression