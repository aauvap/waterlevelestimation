python testCNN.py --data ./VA --model alexnet --mode Class2015  --batch-size 64  --resume ./WaterLevel_runs/alexnet_Class2015_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Class2015 --batch-size 64  --resume ./WaterLevel_runs/resnet18_Class2015_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Class2015 --batch-size 64  --resume ./WaterLevel_runs/resnet34_Class2015_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Class2015 --batch-size 64  --resume ./WaterLevel_runs/resnet50_Class2015_False_0.010000_0.000100

python testCNN.py --data ./VA --model alexnet --mode Class2015 --pretrained --batch-size 64  --resume ./WaterLevel_runs/alexnet_Class2015_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Class2015 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet18_Class2015_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Class2015 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet34_Class2015_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Class2015 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet50_Class2015_True_0.001000_0.000100

python testCNN.py --data ./VA --model alexnet --mode Class2011  --batch-size 64  --resume ./WaterLevel_runs/alexnet_Class2011_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Class2011 --batch-size 64  --resume ./WaterLevel_runs/resnet18_Class2011_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Class2011 --batch-size 64  --resume ./WaterLevel_runs/resnet34_Class2011_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Class2011 --batch-size 64  --resume ./WaterLevel_runs/resnet50_Class2011_False_0.010000_0.000100

python testCNN.py --data ./VA --model alexnet --mode Class2011 --pretrained --batch-size 64  --resume ./WaterLevel_runs/alexnet_Class2011_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Class2011 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet18_Class2011_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Class2011 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet34_Class2011_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Class2011 --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet50_Class2011_True_0.001000_0.000100

python testCNN.py --data ./VA --model alexnet --mode Regression --pretrained --batch-size 64  --resume ./WaterLevel_runs/alexnet_Regression_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Regression --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet18_Regression_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Regression --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet34_Regression_True_0.001000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Regression --pretrained --batch-size 64  --resume ./WaterLevel_runs/resnet50_Regression_True_0.001000_0.000100

python testCNN.py --data ./VA --model alexnet --mode Regression  --batch-size 64  --resume ./WaterLevel_runs/alexnet_Regression_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet18 --mode Regression --batch-size 64  --resume ./WaterLevel_runs/resnet18_Regression_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet34 --mode Regression --batch-size 64  --resume ./WaterLevel_runs/resnet34_Regression_False_0.010000_0.000100
python testCNN.py --data ./VA --model resnet50 --mode Regression --batch-size 64  --resume ./WaterLevel_runs/resnet50_Regression_False_0.010000_0.000100