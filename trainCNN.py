import argparse
import os
import numpy as np
import shutil
import time
import torch
from torch import optim
import torch.nn as nn
from torch.utils.data import DataLoader
import torchvision.transforms as T
import torchvision.models as models
import pickle as pkl
import json

from dataloader import WaterClassificationDataset, WaterClassificationRegression


def train(train_loader, model, criterion, optimizer, epoch, metric_func, device, args):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Metric', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1],
        prefix="Epoch: [{}]".format(epoch))

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target, imgPath) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        images = images.to(device)
        target = target.to(device)
        if args.mode == "Regression":
            target = target.float()
        else:
            target = target.long() 

        # compute output
        output = model(images)
        loss = criterion(output, target)

        # measure accuracy and record loss
        metric = metric_func(output, target)
        losses.update(loss.item(), images.size(0))
        top1.update(metric[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % args.print_freq == 0:
            progress.display(i)

    return top1.avg, losses.avg


def validate(val_loader, model, criterion, metric_func, device, args):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Metric', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target, imgPath) in enumerate(val_loader):

            images = images.to(device)
            target = target.to(device)

            # compute output
            output = model(images)
            loss = criterion(output, target)

            # measure accuracy and record loss
            metric = metric_func(output, target)
            losses.update(loss.item(), images.size(0))
            top1.update(metric[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % args.print_freq == 0:
                progress.display(i)

    return top1.avg, losses.avg


def save_checkpoint(state, ouputDir, is_best_metric, is_best_loss, filename='checkpoint.pth.tar'):
    torch.save(state, os.path.join(ouputDir, filename))
    if is_best_metric:
        shutil.copyfile(os.path.join(ouputDir, filename), os.path.join(ouputDir, 'model_best_metric.pth.tar'))
    if is_best_loss:
        shutil.copyfile(os.path.join(ouputDir, filename), os.path.join(ouputDir, 'model_best_loss.pth.tar'))


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def accuracy(output, target):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        batch_size = target.size(0)

        _, pred = output.topk(1, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        correct_k = correct[:1].reshape(-1).float().sum(0, keepdim=True)
        res = correct_k.mul_(100.0 / batch_size)
    return [res.item()]


def mse(output, target):
    """Computes the mse"""
    with torch.no_grad():
        output = torch.clamp(output, min=0, max=100)
        mse = torch.mean(torch.square(target-output))
    return [mse.item()]






def main(args):


    args.exp_name = "{}_{}_{}_{:.6f}_{:.6f}".format(args.model, args.mode, args.pretrained, args.lr, args.weight_decay)

    outputDir = os.path.join(".", "WaterLevel_runs", args.exp_name)

    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)
    else:
        print("{} already exists. Exiting".format(args.exp_name))
        exit()

    args.cuda =  torch.cuda.is_available()
    if args.cuda:
        print('Using GPU : ' + str(torch.cuda.current_device()) + ' from ' + str(torch.cuda.device_count()) + ' devices')
    else:
        print('Using CPU')
                           
    device = torch.device("cuda" if args.cuda else "cpu")
    


    # Transforms

    if args.pretrained:
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
    else:
        mean = [0.517, 0.473, 0.374]
        std = [0.191, 0.185, 0.149]

    train_transform = T.Compose([T.Resize((224,224)), T.RandomHorizontalFlip(), T.ToTensor(), T.Normalize(mean=mean, std=std)])
    valid_transform = T.Compose([T.Resize((224,224)), T.ToTensor(), T.Normalize(mean=mean, std=std)])

    # Datasets

    if args.mode == "Class2011":
        train_dataset = WaterClassificationDataset(root = args.data, split="Train", transform=train_transform, waterIntervals=[])
        valid_dataset = WaterClassificationDataset(root = args.data, split="Val", transform=valid_transform, waterIntervals=[])
        
        weight_tensor = torch.from_numpy(train_dataset.class_weights).float().to(device)
        criterion = nn.CrossEntropyLoss(weight=weight_tensor)
        num_classes = train_dataset.numWaterClasses

        metric_func = accuracy

    elif args.mode == "Class2015":
        train_dataset = WaterClassificationDataset(root = args.data, split="Train", transform=train_transform, waterIntervals=[5, 15, 30])
        valid_dataset = WaterClassificationDataset(root = args.data, split="Val", transform=valid_transform, waterIntervals=[5, 15, 30])

        weight_tensor = torch.from_numpy(train_dataset.class_weights).float().to(device)
        criterion = nn.CrossEntropyLoss(weight=weight_tensor)
        num_classes = train_dataset.numWaterClasses

        metric_func = accuracy

    elif args.mode == "Regression":
        train_dataset = WaterClassificationRegression(root = args.data, split="Train", transform=train_transform)
        valid_dataset = WaterClassificationRegression(root = args.data, split="Val", transform=valid_transform)

        criterion = nn.MSELoss()
        num_classes = 1

        metric_func = mse
    else:
        raise "Unexpected dataset mode: {}".format(args.mode)
    
    # Dataloaders
    train_dataloader = DataLoader(train_dataset, num_workers=args.num_workers, batch_size=args.batch_size, shuffle=True, drop_last=True)
    valid_dataloader = DataLoader(valid_dataset, num_workers=args.num_workers, batch_size=args.batch_size, shuffle=True, drop_last=False)


    # Model

    model_type = args.model.lower()

    if model_type == "alexnet":

        if args.pretrained:
            model = models.alexnet(pretrained = True)
            model.classifier = nn.Sequential(
                nn.Dropout(),
                nn.Linear(256*6*6, 4096),
                nn.ReLU(inplace=True),
                nn.Dropout(),
                nn.Linear(4096,4096),
                nn.ReLU(inplace=True),
                nn.Linear(4096, num_classes)
            )
        else:
            model = models.alexnet(False, **{"num_classes": num_classes})
    
    elif model_type == "resnet18":
        if args.pretrained:
            model = models.resnet18(True)
            model.fc = nn.Linear(512, num_classes)
        else:
            model = models.resnet18(False,**{"num_classes": num_classes})

    elif model_type == "resnet34":
        if args.pretrained:
            model = models.resnet34(True)
            model.fc = nn.Linear(512, num_classes)
        else:
            model = models.resnet34(False, **{"num_classes": num_classes})

    elif model_type == "resnet50":
        if args.pretrained:
            model = models.resnet50(True)
            model.fc = nn.Linear(512 * 4, num_classes)
        else:
            model = models.resnet50(False, **{"num_classes": num_classes})

    else:
        raise "Unknown model".format(model_type)
    
    # Freezing Resnet Residual blocks
    if args.pretrained and "resnet" in model_type:
        layers_to_train = ["layer4", "layer3", "layer2", "layer1", "conv1"][:args.trainable_layers]

        for name, parameter in model.named_parameters():
            if all([not name.startswith(layer) for layer in layers_to_train]):
                parameter.requires_grad_(False)

    model = model.to(device)


    # Optimizer
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)

    # LR Scheduler
    #scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, args.epochs, eta_min=args.lr/100)

    if args.evaluate:
        validate(valid_dataloader, model, criterion, metric_func, device, args)
        return

    best_metric = 0
    best_loss = np.inf


    with open(os.path.join(outputDir, "Settings.json"), 'w') as fp:
        json.dump(vars(args), fp)

    results = {"TrainMetric": [], "TrainLoss": [], "ValLoss": [], "ValMetric": []}
    for epoch in range(args.start_epoch, args.epochs):

        # train for one epoch
        metric_train, loss_train = train(train_dataloader, model, criterion, optimizer, epoch, metric_func, device, args)

        #scheduler.step()

        # evaluate on validation set
        metric, loss = validate(valid_dataloader, model, criterion, metric_func, device, args)

        results["TrainMetric"].append(metric_train)
        results["TrainLoss"].append(loss_train)
        results["ValMetric"].append(metric)
        results["ValLoss"].append(loss)

        # remember best acc@1 and save checkpoint
        is_best_metric = metric > best_metric
        best_metric = max(metric, best_metric)

        is_best_loss = loss < best_loss
        best_loss = min(loss, best_loss)

        save_checkpoint({
            'epoch': epoch + 1,
            'arch': args.model,
            'state_dict': model.state_dict(),
            'best_metric': best_metric,
            'best_loss': best_loss,
            'optimizer' : optimizer.state_dict(),
        }, outputDir, is_best_metric, is_best_loss, filename='checkpoint_{}.pth.tar'.format(epoch))

        with open(os.path.join(outputDir, "Results.json"), 'w') as fp:
            json.dump(results, fp)




if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
    parser.add_argument('--data', metavar='DIR',
                        help='path to dataset')

    parser.add_argument('-m', '--model', metavar='ARCH', default='resnet18',
                        choices=["alexnet", "resnet18", "resnet34", "resnet50"],
                        help='model architecture: ' +
                            ' | '.join(["alexnet", "resnet18", "resnet34", "resnet50"]) +
                            ' (default: resnet18)')


    parser.add_argument('-mode', '--mode', metavar='Mode', default='Class2011',
                        choices=["Class2011", "Class2015", "Regression"],
                        help='Training mode: ' +
                            ' | '.join(["Class2011", "Class2015", "Regression"]) +
                            ' (default: Class2011)')


    parser.add_argument(
        "--num-workers",
        metavar="N",
        type=int,
        default=None,
        help="Number of workers for the image loading. Defaults to the number of CPUs.",
    )


    parser.add_argument(
        "--seed",
        metavar="S",
        type=int,
        default=None,
        help="If given, runs the calculation in deterministic mode with manual seed S.",
    )

    parser.add_argument('--epochs', default=90, type=int, metavar='N',
                        help='number of total epochs to run')
    parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                        help='manual epoch number (useful on restarts)')
    parser.add_argument('-b', '--batch-size', default=256, type=int,
                        metavar='N',
                        help='mini-batch size (default: 256), this is the total '
                            'batch size of all GPUs on the current node when '
                            'using Data Parallel or Distributed Data Parallel')
    parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                        metavar='LR', help='initial learning rate', dest='lr')
    parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                        help='momentum')
    parser.add_argument('--wd', '--weight-decay', default=1e-4, type=float,
                        metavar='W', help='weight decay (default: 1e-4)',
                        dest='weight_decay')


    parser.add_argument('-tl', '--trainable_layers', default=3, type=int)

    parser.add_argument('-p', '--print-freq', default=35, type=int,
                        metavar='N', help='print frequency (default: 10)')

    parser.add_argument('--resume', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')

    parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                        help='evaluate model on validation set')
    parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                        help='use pre-trained model')

    args = parser.parse_args()

    if args.num_workers is None:
        args.num_workers = 8

    main(args)