import os
import numpy as np
import pandas as pd
import time as time 
import argparse
import pickle

def loadData(path, split):
    df = pd.read_csv(os.path.join(".", "VAData", "ALL_VA_{}.csv".format(split)), sep=",")

    gist = np.load(os.path.join(".", "VAData", "{}_Data.npy".format(split)))
    files = np.load(os.path.join(".", "VAData", "{}_Filename.npy".format(split)))
    labels = np.asarray([df[df["Filename"] == x]["WaterLevel"].values[0] for x in files])

    print(gist.shape, labels.shape, files.shape)

    print(np.allclose(gist[0,:], gist[120,:]))

    return gist, labels, files

def convertToClass2015(labels, intervals):
    labels[labels < intervals[0]] = 0
    labels[labels>= intervals[-1]] = len(intervals)

    for idx in range(1, len(intervals)):
        labels[(labels >= intervals[idx-1]) & (labels < intervals[idx])] = idx

    return labels


def main(args):
    modelPath = args["modelPath"]
    outputDir = args["outputDir"]
    mode = args["mode"]
    dataDir = os.path.join(".", "VAData")


    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)

    gistTrain, labelsTrain, filesTrain = loadData(dataDir, "Train")
    gistVal, labelsVal, filesVal = loadData(dataDir, "Val")
    gistTest, labelsTest, filesTest = loadData(dataDir, "Test")


    if mode == "Class2015":
        intervals = [5, 15, 30]
        labelsTrain = convertToClass2015(labelsTrain, intervals)
        labelsVal = convertToClass2015(labelsVal, intervals)
        labelsTest = convertToClass2015(labelsTest, intervals)
    elif mode == "Class2011":
        uniqueLevels = np.unique(labelsTrain)
        for idx, level in enumerate(uniqueLevels):
            labelsTrain[labelsTrain == level] = idx
            labelsVal[labelsVal == level] = idx
            labelsTest[labelsTest == level] = idx
    
    
    with open(os.path.join(modelPath, "best_model_loss.pkl"), 'rb') as fid:
        forest = pickle.load(fid) 

    start_time = time.time()

    forest.fit(gistTrain, labelsTrain)

    trainPrediction = forest.predict(gistTrain)
    valPrediction = forest.predict(gistVal)
    testPrediction = forest.predict(gistTest)

    df = pd.DataFrame({"Fileame": filesTest, "Prediction": testPrediction, "Label": labelsTest})

    expname = os.path.basename(os.path.normpath(modelPath))
    df.to_csv(os.path.join(outputDir, "{}.csv".format(expname)), sep=",", index=False)

    end_time = time.time()

    print("\nTime spent: {}".format(end_time-start_time))

    print(testPrediction)

    

if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("--modelPath", type=str, default=None)
    ap.add_argument("--outputDir", type=str, default=None)
    ap.add_argument("--mode", default = "Class2011", choices=["Class2011", "Class2015", "Regression"])

    args = vars(ap.parse_args())
    main(args)