## Water Level Estimation in Sewer Pipes using Deep Convolutional Neural Networks

This repository contains the code and scripts for the paper *Water Level Estimation in Sewer Pipes using Deep Convolutional Neural Networks*

The paper investigates estimating the water level in sewer pipes, from a single RGB images. Four CNN based methods (AlexNet, ResNet-18, Resnet-34, ResNet-50)and two decision tree methods (Random Forest, Extra Trees) with the GIST feature descriptor are compared. The methods are compared on a pre-determined datasplit on class balanced data, using the macro and micro F1 scores. We compare performance when using annotating the data according to the 2010 version of the Danish Sewer Inspection Standard, and with the simplified 2015 vesion of the Standard. Lastly, we investigate how training in a regression framework affects the results.

As this is a collection of research code, one might find some occasional rough edges. We have tried to clean up the code to a decent level but if you encounter a bug or a regular mistake, please report it in our issue tracker. 


### Code references

The GIST feature descriptor code was based on the LEAR GIST python wrapper implementation of [Yuichiro Tsuciya](https://github.com/tuttieee/lear-gist-python).

### License

All code is licensed under the MIT license, except for the aforementioned code reference, which are subject to their respective licenses when applicable.

### Acknowledgements
Please cite the following paper if you use our code or method:


```TeX
@article{Haurum_2020_WLE, 
title={Water Level Estimation in Sewer Pipes Using Deep Convolutional Neural Networks}, 
volume={12}, 
ISSN={2073-4441},
DOI={10.3390/w12123412},
number={12},
journal={Water},
author={Haurum, Joakim Bruslund and Bahnsen, Chris H. and Pedersen, Malte and Moeslund, Thomas B.},
year={2020},
month={Dec}}
```
