import argparse
import os
import numpy as np
import shutil
import time
import torch

import torch.nn as nn
from torch.utils.data import DataLoader
import torchvision.transforms as T
import torchvision.models as models
import pandas as pd

from dataloader import WaterClassificationDataset, WaterClassificationRegression


def validate(val_loader, model, device, args):

    # switch to evaluate mode
    model.eval()

    imgPaths = []
    predictions = []
    targets = []

    with torch.no_grad():
        end = time.time()
        for i, (images, target, imgPath) in enumerate(val_loader):

            images = images.to(device)

            # compute output
            output = model(images)

            if args.mode == "Regression":
                output = torch.clamp(output[:,0], min=0, max=100)
                target = target[:,0]
            else:
                output = torch.argmax(output, dim=1)

            imgPath = list(imgPath)
            imgPaths.extend(imgPath)
            
            output = list(output.detach().cpu().numpy())
            predictions.extend(output)

            target = list(target.cpu().numpy())
            targets.extend(target)

    return imgPaths, predictions, targets



def main(args):


    args.cuda =  torch.cuda.is_available()
    if args.cuda:
        print('Using GPU : ' + str(torch.cuda.current_device()) + ' from ' + str(torch.cuda.device_count()) + ' devices')
    else:
        print('Using CPU')
                           
    device = torch.device("cuda" if args.cuda else "cpu")
    


    # Transforms

    if args.pretrained:
        mean = [0.485, 0.456, 0.406]
        std = [0.229, 0.224, 0.225]
    else:
        mean = [0.517, 0.473, 0.374]
        std = [0.191, 0.185, 0.149]

    test_transform = T.Compose([T.Resize((224,224)), T.ToTensor(), T.Normalize(mean=mean, std=std)])

    # Datasets

    if args.mode == "Class2011":
        test_dataset = WaterClassificationDataset(root = args.data, split="Test", transform=test_transform, waterIntervals=[])
        num_classes = test_dataset.numWaterClasses

    elif args.mode == "Class2015":
        test_dataset = WaterClassificationDataset(root = args.data, split="Test", transform=test_transform, waterIntervals=[5, 15, 30])
        num_classes = test_dataset.numWaterClasses

    elif args.mode == "Regression":
        test_dataset = WaterClassificationRegression(root = args.data, split="Test", transform=test_transform)
        num_classes = 1
    else:
        raise "Unexpected dataset mode: {}".format(args.mode)
    
    # Dataloaders
    test_loader = DataLoader(test_dataset, num_workers=args.num_workers, batch_size=args.batch_size, shuffle=True, drop_last=False)


    # Model

    model_type = args.model.lower()

    if model_type == "alexnet":
        model = models.alexnet(False, **{"num_classes": num_classes})
    elif model_type == "resnet18":
        model = models.resnet18(False,**{"num_classes": num_classes})
    elif model_type == "resnet34":
        model = models.resnet34(False, **{"num_classes": num_classes})
    elif model_type == "resnet50":
        model = models.resnet50(False, **{"num_classes": num_classes})
    else:
        raise "Unknown model".format(model_type)
    

    # optionally resume from a checkpoint
    if args.resume:
        resumePath = os.path.join(args.resume, "model_best_loss.pth.tar")
        if os.path.isfile(resumePath):
            print("=> loading checkpoint '{}'".format(resumePath))
            checkpoint = torch.load(resumePath)
            model.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint '{}' (epoch {})".format(resumePath, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(resumePath))
            exit(0)



    model = model.to(device)

    filepaths, predictions, labels = validate(test_loader, model, device, args)

    outputDir = os.path.dirname(args.resume)

    df = pd.DataFrame({"Fileame": filepaths, "Prediction": predictions, "Label": labels})
    #df.to_csv(os.path.join(outputDir, "TestResults.csv"), sep=",", index=False)

    expname = os.path.basename(os.path.normpath(args.resume))
    df.to_csv(os.path.join(".", "results", "{}.csv".format(expname)), sep=",", index=False)

    



if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
    parser.add_argument('--data', metavar='DIR',
                        help='path to dataset')

    parser.add_argument('-m', '--model', metavar='ARCH', default='resnet18',
                        choices=["alexnet", "resnet18", "resnet34", "resnet50"],
                        help='model architecture: ' +
                            ' | '.join(["alexnet", "resnet18", "resnet34", "resnet50"]) +
                            ' (default: resnet18)')


    parser.add_argument('-mode', '--mode', metavar='Mode', default='Class2011',
                        choices=["Class2011", "Class2015", "Regression"],
                        help='Training mode: ' +
                            ' | '.join(["Class2011", "Class2015", "Regression"]) +
                            ' (default: Class2011)')


    parser.add_argument(
        "--num-workers",
        metavar="N",
        type=int,
        default=None,
        help="Number of workers for the image loading. Defaults to the number of CPUs.",
    )
    parser.add_argument('-b', '--batch-size', default=256, type=int,
                        metavar='N',
                        help='mini-batch size (default: 256), this is the total '
                            'batch size of all GPUs on the current node when '
                            'using Data Parallel or Distributed Data Parallel')
    parser.add_argument('-p', '--print-freq', default=35, type=int,
                        metavar='N', help='print frequency (default: 10)')

    parser.add_argument('--resume', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')

    parser.add_argument('--pretrained', dest='pretrained', action='store_true',
                        help='use pre-trained model')
    args = parser.parse_args()

    if args.num_workers is None:
        args.num_workers = 8

    main(args)