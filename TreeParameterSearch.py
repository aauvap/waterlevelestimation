import os
import numpy as np
import pandas as pd
import time as time 
import argparse
import pickle

from sklearn.ensemble import ExtraTreesClassifier, ExtraTreesRegressor, RandomForestClassifier, RandomForestRegressor

def loadData(path, split):
    df = pd.read_csv(os.path.join(".", "VAData", "ALL_VA_{}.csv".format(split)), sep=",")

    gist = np.load(os.path.join(".", "VAData", "{}_Data.npy".format(split)))
    files = np.load(os.path.join(".", "VAData", "{}_Filename.npy".format(split)))
    labels = np.asarray([df[df["Filename"] == x]["WaterLevel"].values[0] for x in files])

    print(gist.shape, labels.shape, files.shape)

    print(np.allclose(gist[0,:], gist[120,:]))

    return gist, labels, files

def safe_log(x, eps=1e-10): 
    result = np.where(x > eps, x, -10) 
    np.log(result, out=result, where=result > 0)
    return result

def ceLoss(output, target, weights):
    logOutput = safe_log(output)
    relevantElements = -logOutput[np.arange(output.shape[0]), target]

    for class_key in weights.keys():
        relevantElements[target == int(class_key)] = relevantElements[target == int(class_key)]* weights[class_key]
        
    
    return np.mean(relevantElements)




def accuracy(output, target):
    binaryMatch = output == target
    matches = np.sum(binaryMatch)
    accuracy = matches *  100./target.shape[0]
    return accuracy

    
def mse(output, target):
    output = np.clip(output, 0, 100)
    diff = output  - target
    squaredDiff = diff**2
    mse = np.mean(squaredDiff)
    return mse

def convertToClass2015(labels, intervals):
    labels[labels < intervals[0]] = 0
    labels[labels>= intervals[-1]] = len(intervals)

    for idx in range(1, len(intervals)):
        labels[(labels >= intervals[idx-1]) & (labels < intervals[idx])] = idx

    return labels


def main(args):
    mode = args["mode"]
    extraTrees = args["extraTrees"]
    dataDir = os.path.join(".", "VAData")

    if extraTrees:
        outputDir = os.path.join(".", "TreeMethod", "ExtraTree" + "-" + mode)
    else:
        outputDir = os.path.join(".", "TreeMethod", "RandomForest" + "-" + mode)

    if not os.path.isdir(outputDir):
        os.makedirs(outputDir)

    gistTrain, labelsTrain, filesTrain = loadData(dataDir, "Train")
    gistVal, labelsVal, filesVal = loadData(dataDir, "Val")
    gistTest, labelsTest, filesTest = loadData(dataDir, "Test")


    if mode == "Class2015":
        intervals = [5, 15, 30]
        labelsTrain = convertToClass2015(labelsTrain, intervals)
        labelsVal = convertToClass2015(labelsVal, intervals)
        labelsTest = convertToClass2015(labelsTest, intervals)
    elif mode == "Class2011":
        uniqueLevels = np.unique(labelsTrain)
        for idx, level in enumerate(uniqueLevels):
            labelsTrain[labelsTrain == level] = idx
            labelsVal[labelsVal == level] = idx
            labelsTest[labelsTest == level] = idx

    trainUniqueLabels, trainLabelCounts = np.unique(labelsTrain, return_counts=True)
    class_weights = {idx: np.max(trainLabelCounts)/x for idx, x in enumerate(trainLabelCounts)}


    if mode == "Regression":
        if extraTrees:
            model = ExtraTreesRegressor
        else:
            model = RandomForestRegressor
        metricFunc = mse
    else:
        if extraTrees:
            model = ExtraTreesClassifier
        else:
            model = RandomForestClassifier
        metricFunc = accuracy

    results = {}
    best_val_metric = np.inf if mode == "Regression" else 0
    best_val_loss = np.inf
    best_mode_metric = None
    best_mode_loss = None

    start_time = time.time()
    for n_estimators in [10, 100, 250]:
        for max_depth in [10, 20, 30]:
            for max_features in ["sqrt", "log2", 512//3, None]:

                if mode == "Regression":
                    forest = model(n_estimators=n_estimators, max_depth=max_depth, max_features=max_features, n_jobs=-1, random_state=0, min_samples_leaf = 5)
                else:
                    forest = model(n_estimators=n_estimators, max_depth=max_depth, max_features=max_features, n_jobs=-1, random_state=0, class_weight=class_weights, min_samples_leaf = 1)
                

                forest.fit(gistTrain, labelsTrain)

                trainPrediction = forest.predict(gistTrain)
                valPrediction = forest.predict(gistVal)

                trainMetric = metricFunc(trainPrediction, labelsTrain)
                valMetric = metricFunc(valPrediction, labelsVal)

                if mode == "Regression": 
                    trainLoss = trainMetric
                    valLoss = valMetric 

                    if valMetric < best_val_metric:
                        best_val_metric = valMetric
                        best_mode_metric = "{}_{}_{}".format(n_estimators, max_depth, max_features)
                        with open(os.path.join(outputDir, "best_model_metric.pkl"), 'wb') as fid:
                            pickle.dump(forest, fid)   

                else: 
                    trainProb = forest.predict_proba(gistTrain)
                    valProb = forest.predict_proba(gistVal)

                    trainLoss = ceLoss(trainProb, labelsTrain, class_weights)
                    valLoss = ceLoss(valProb, labelsVal, class_weights)

                    if valMetric > best_val_metric:
                        best_val_metric = valMetric
                        best_mode_metric = "{}_{}_{}".format(n_estimators, max_depth, max_features)
                        with open(os.path.join(outputDir, "best_model_metric.pkl"), 'wb') as fid:
                            pickle.dump(forest, fid) 
                
                if valLoss < best_val_loss:
                    best_val_loss = valLoss
                    best_mode_loss = "{}_{}_{}".format(n_estimators, max_depth, max_features)

                    
                    with open(os.path.join(outputDir, "best_model_loss.pkl"), 'wb') as fid:
                        pickle.dump(forest, fid) 

                tag = "{}_{}_{}".format(n_estimators, max_depth, max_features)
                results[tag] = {"ID": tag, "Trees": n_estimators, "Depth": max_depth, "Features": max_features, "TrainMetric": trainMetric, "ValMetric": valMetric, "TrainLoss": trainLoss, "ValLoss": valLoss}
                

    end_time = time.time()

    print("\nTime spent: {}".format(end_time-start_time))

    df = pd.DataFrame.from_dict(results, orient="index")
    df = df.sort_values(["Trees", "Depth", "Features"])
    print(df)

    df.to_csv(os.path.join(outputDir, "searchResults.csv"), sep=",", index=False)

    with open(os.path.join(outputDir, "bestModels.txt"), "w") as f:
        f.write("Best Loss Model: {}: TrnLoss {} - ValLoss {} - TrnMetric {} - ValMetric {}\n".format(best_mode_loss, results[best_mode_loss]["TrainLoss"], results[best_mode_loss]["ValLoss"], results[best_mode_loss]["TrainMetric"], results[best_mode_loss]["ValMetric"]))
        f.write("Best Metric Model: {}: TrnLoss {} - ValLoss {} - TrnMetric {} - ValMetric {}".format(best_mode_metric, results[best_mode_metric]["TrainLoss"], results[best_mode_metric]["ValLoss"], results[best_mode_metric]["TrainMetric"], results[best_mode_metric]["ValMetric"]))


if __name__ == "__main__":
    ap = argparse.ArgumentParser()

    ap.add_argument("--mode", default = "Class2011", choices=["Class2011", "Class2015", "Regression"])
    ap.add_argument("--extraTrees", action="store_true")

    args = vars(ap.parse_args())
    main(args)